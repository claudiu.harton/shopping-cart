const express = require("express");
const router = express.Router();

const { isAuthenticated } = require("../controllers").auth.middleware;
const { products } = require("../controllers");

router.get("/", products.getProducts);

router.post("/", products.addProduct);

router.delete("/", products.removeProduct);

router.put("/", products.editProduct);

router.post("/reviews", isAuthenticated, products.addReview);

module.exports = router;
