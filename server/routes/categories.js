const express = require("express");
const router = express.Router();

const { categories } = require("../controllers");

router.get("/", categories.getCategories);

router.post("/", categories.addCategory);

router.delete("/", categories.removeCategory);

module.exports = router;
