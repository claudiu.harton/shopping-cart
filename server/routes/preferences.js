const express = require("express");
const router = express.Router();

const { preferences } = require("../controllers");

router.put("/", preferences.editPreferences);
router.get("/", preferences.getPreferences);

module.exports = router;
