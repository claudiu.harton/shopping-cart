const Sequelize = require("sequelize");
const configuration = require("../../configuration");

const { db } = process.env.PROD
  ? configuration.prod
  : process.env.COMPUTERNAME == "BUK30LAP4001541"
  ? configuration.dev2
  : configuration.dev;

const sequelize = new Sequelize(db.database, db.userName, db.password, {
  dialect: db.dialect,
  host: db.host,
});

module.exports = sequelize;
