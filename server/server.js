const express = require("express");
const router = require("./routes");
const bodyParser = require("body-parser");
const passport = require("passport");
const serveStatic = require("serve-static");
const history = require("connect-history-api-fallback");
const { sequelize, User, Preference } = require("./models");
const { session } = require("./config");
const bcrypt = require("bcryptjs");
const saltRounds = 10;

const app = express();
bcrypt.genSalt(saltRounds, (err, salt) => {
  bcrypt.hash("admin", salt, async (err, hash) => {
    try {
      await sequelize.sync();
      console.log("Database syncronized");
      const user = await User.findOne({ where: { email: "admin" } });
      if (!user) {
        await User.create({
          firstName: "Admin",
          lastName: "Admin",
          email: "admin",
          isAdmin: true,
          token: Math.random().toString(36),
          password: hash,
        });
        console.log("Admin user was created");
      }

      const activatedVouchers = await Preference.findOne({
        where: { name: "activatedVouchers" },
      });
      if (!activatedVouchers) {
        await Preference.create({
          name: "activatedVouchers",
          value: "[]",
        });
        console.log("activatedVouchers was created");
      }

      const brandName = await Preference.findOne({
        where: { name: "brandName" },
      });
      if (!brandName) {
        await Preference.create({
          name: "brandName",
          value: "YouFashion",
        });
        console.log("brandName was created");
      }

      const noItems = await Preference.findOne({
        where: { name: "noItems" },
      });
      if (!noItems) {
        await Preference.create({
          name: "noItems",
          value: "3",
        });
        console.log("noItems was created");
      }

      const FAQ = await Preference.findOne({
        where: { name: "FAQ" },
      });
      if (!FAQ) {
        await Preference.create({
          name: "FAQ",
          value: "De completat",
        });
        console.log("FAQ was created");
      }

      const terms = await Preference.findOne({
        where: { name: "Terms & Conditions" },
      });
      if (!terms) {
        await Preference.create({
          name: "Terms & Conditions",
          value: "De completat",
        });
        console.log("Terms & Conditions was created");
      }

      const about = await Preference.findOne({
        where: { name: "About Us" },
      });
      if (!about) {
        await Preference.create({
          name: "About Us",
          value: "De completat",
        });
        console.log("About Us was created");
      }
    } catch (error) {
      console.error(error);
    }
  });
});
const configure = (app) => {
  app.use(bodyParser.json());
  app.use(session);
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(history());
  app.use(serveStatic(__dirname + "/../client/dist/spa"));

  app.use("/api", router);
};

module.exports = configure;

configure(app);

const port = process.env.PORT || 8081;
app.listen(port, () =>
  console.log(`I am running on port ${port}: http://localhost:${port}`)
);
