module.exports = function (sequelize, DataTypes) {
  return sequelize.define("review", {
    text: DataTypes.STRING,
  });
};
