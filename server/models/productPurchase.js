module.exports = function (sequelize, DataTypes) {
  return sequelize.define("productPurchase", {
    quantity: DataTypes.INTEGER,
  });
};
