module.exports = function (sequelize, DataTypes) {
  return sequelize.define("voucher", {
    code: DataTypes.STRING,
    type: DataTypes.STRING,
    value: DataTypes.FLOAT,
    limit: DataTypes.INTEGER,
  });
};
