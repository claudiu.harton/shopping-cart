const { sequelize } = require("../config");

const User = sequelize.import("./user");
const Preference = sequelize.import("./preference");
const Product = sequelize.import("./product");
const ProductPreference = sequelize.import("./productPreference");
const ProductPurchase = sequelize.import("./productPurchase");
const Review = sequelize.import("./review");
const Purchase = sequelize.import("./purchase");
const ShoppingCart = sequelize.import("./shoppingCart");
const Voucher = sequelize.import("./voucher");
const Category = sequelize.import("./category");

User.hasOne(ShoppingCart, { onDelete: "Cascade" });
User.hasMany(Purchase, { onDelete: "Cascade" });
User.hasMany(Preference, { onDelete: "Cascade" });
User.hasMany(Review, { onDelete: "Cascade" });

ProductPreference.hasMany(ShoppingCart, { onDelete: "Cascade" });
ProductPreference.hasMany(ProductPurchase, { onDelete: "Cascade" });

Product.hasMany(ProductPreference, { onDelete: "Cascade" });
Product.hasOne(Review, { onDelete: "Cascade" });

Voucher.hasOne(Purchase, { onDelete: "Cascade" });
Purchase.hasMany(ProductPurchase, { onDelete: "Cascade" });

Category.hasMany(Product, { onDelete: "Cascade" });

module.exports = {
  User,
  Preference,
  Product,
  ProductPreference,
  ProductPurchase,
  Review,
  Purchase,
  ShoppingCart,
  Voucher,
  Category,
  sequelize,
};
