module.exports = function (sequelize, DataTypes) {
  return sequelize.define("productPreference", {
    size: DataTypes.STRING,
    color: DataTypes.STRING,
    quantityAvailable: DataTypes.INTEGER,
  });
};
