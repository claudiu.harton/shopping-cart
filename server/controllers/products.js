const {
  Product,
  Category,
  ProductPreference,
  Review,
  User,
} = require("../models");

const controller = {
  getProducts: async (req, res) => {
    try {
      const products = await Product.findAll({
        attributes: [
          "id",
          "name",
          "price",
          "description",
          "categoryId",
          "image",
        ],
        raw: true,
      });

      const productsFormatted = await Promise.all(
        products.map(async (product) => {
          const reviewsRaw = await Review.findAll({
            attributes: ["id", "text", "productId", "userId"],
            where: { productId: product.id },

            raw: true,
          });

          const reviews = await Promise.all(
            reviewsRaw.map(async (review) => {
              const user = await User.findOne({
                attributes: ["id", "firstName", "lastName"],
                where: { id: review.userId },
                raw: true,
              });

              return { ...review, user };
            })
          );

          product.preferences = await ProductPreference.findAll({
            attributes: ["id", "size", "color", "quantityAvailable"],
            where: { productId: product.id },
            raw: true,
          });

          const category = await Category.findOne({
            attributes: ["id", "name"],
            where: { id: product.categoryId },
            raw: true,
          });

          return { ...product, category, reviews };
        })
      );

      res.status(200).send(productsFormatted);
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  addProduct: async (req, res) => {
    try {
      const {
        name,
        price,
        description,
        categoryId,
        preferences,
        image,
      } = req.body;

      const category = await Category.findOne({
        where: { id: categoryId },
        raw: true,
      });

      if (category) {
        const product = await Product.create({
          name,
          price,
          description,
          image,
          categoryId,
        });

        await Promise.all(
          preferences.map(async (item) => {
            await ProductPreference.create({
              size: item.size,
              color: item.color,
              quantityAvailable: item.quantityAvailable,
              productId: product.id,
            });
          })
        );

        res.status(200).send({ message: "Product created" });
      } else {
        res.status(404).send({ message: "Category doesn't exists" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  editProduct: async (req, res) => {
    try {
      const {
        id,
        name,
        price,
        description,
        categoryId,
        preferences,
        image,
      } = req.body;

      const productBefore = await Product.findOne({ where: { id } });

      if (productBefore) {
        const category = await Category.findOne({
          where: { id: categoryId },
          raw: true,
        });

        if (category) {
          const product = await productBefore.update({
            ...productBefore,
            name: name ? name : productBefore.name,
            price: price ? price : productBefore.price,
            image: image ? image : productBefore.image,
            description: description ? description : productBefore.description,
            categoryId,
          });

          const productPreferences = await ProductPreference.findAll({
            attributes: ["id", "size", "color", "quantityAvailable"],
            where: { productId: product.id },
            raw: true,
          });

          const toBeRemoved = productPreferences.filter(
            (item) =>
              !preferences.find(
                (pref) => pref.size === item.size && pref.color === item.color
              )
          );

          await Promise.all(
            preferences.map(async (item) => {
              const preference = await ProductPreference.findOne({
                where: { size: item.size, color: item.color },
              });

              if (!preference) {
                await ProductPreference.create({
                  size: item.size,
                  color: item.color,
                  quantityAvailable: item.quantityAvailable,
                  productId: product.id,
                });
              } else {
                await preference.update({
                  ...preference,
                  quantityAvailable: item.quantityAvailable
                    ? item.quantityAvailable
                    : preference.quantityAvailable,
                });
              }
            })
          );

          await Promise.all(
            toBeRemoved.map(async (item) => {
              let preference;
              if (item.id) {
                preference = await ProductPreference.findOne({
                  where: { id: item.id },
                });
              }

              if (preference) {
                await preference.destroy();
              }
            })
          );

          res.status(200).send({ message: "Product edited" });
        } else {
          res.status(404).send({ message: "Category doesn't exists" });
        }
      } else {
        res.status(404).send({ message: "Product doesn't exists" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  removeProduct: async (req, res) => {
    try {
      const { id } = req.body;

      const product = await Product.findOne({ where: { id } });

      if (product) {
        await product.destroy();
        res.status(200).send({ message: "Product deleted" });
      } else {
        res.status(404).send({ message: "Product doesn't exists" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },

  addReview: async (req, res) => {
    try {
      const { text, productId } = req.body;

      const product = await Product.findOne({
        where: { id: productId },
        raw: true,
      });

      if (product) {
        const review = await Review.create({
          text,
          productId,
          userId: req.user.id,
        });

        res.status(200).send({ message: "Review added" });
      } else {
        res.status(404).send({ message: "Product doesn't exists" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
};

module.exports = controller;
