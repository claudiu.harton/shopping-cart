const reset = require("./reset");
const users = require("./users");

const auth = require("./auth");
const categories = require("./categories");
const preferences = require("./preferences");
const products = require("./products");
const purchases = require("./purchases");
const discount = require("./discount");

module.exports = {
  reset,
  users,
  auth,
  categories,
  preferences,
  products,
  purchases,
  discount,
};
