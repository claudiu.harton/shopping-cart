export function getBrandName(state) {
  return state.brandName;
}

export function getNoItems(state) {
  return state.noItems;
}

export function getFaq(state) {
  return state.faq;
}
export function getAboutUs(state) {
  return state.aboutUs;
}
export function getTermsConditions(state) {
  return state.termsConditions;
}

export function getCategories(state) {
  return state.categories;
}
export function getProducts(state) {
  return state.products;
}
export function getPromotions(state) {
  return state.promotions;
}

export function getOnlinePayment(state) {
  return state.onlinePayment;
}

export function getSizes(state) {
  return state.sizes;
}

export function getVoucher(state) {
  return state.voucher;
}

export function getColors(state) {
  return state.colors;
}

export function getShoppingCart(state) {
  return state.shoppingCart;
}
export function getGuestShoppingCart(state) {
  return state.guestShoppingCart;
}

export function getAllVouchers(state) {
  return state.vouchers;
}

export function getCurrentUserProducts(state) {
  return state.currentUserProducts;
}

export function getAllUsersProducts(state) {
  return state.allUsersProducts;
}

export function getAdminPreferences(state) {
  return state.adminPreferences;
}
