import Axios from "axios";

export function setBrandName({ commit }, payload) {
  commit("SET_BRANDNAME", payload);
}

export function setNoItems({ commit }, payload) {
  commit("SET_NOITEMS", payload);
}

export function setDetails({ commit }, payload) {
  commit("SET_DETAILS", payload);
}
export async function loadCategories({ commit }) {
  const response = await Axios.get("/api/categories");
  commit("SET_CATEGORIES", response.data);
}

export async function loadProducts({ commit }) {
  const response = await Axios.get("/api/products");
  commit("SET_PRODUCTS", response.data);
}

export function setPromotions({ commit }, payload) {
  commit("SET_PROMOTIONS", payload);
}

export function setVoucher({ commit }, payload) {
  commit("SET_VOUCHER", payload);
}

export function setOnlinePayment({ commit }, payload) {
  commit("SET_ONLINE_PAYMENT", payload);
}

export function loadSizes({ commit }, payload) {
  commit("SET_SIZES", payload);
}

export function loadColors({ commit }, payload) {
  commit("SET_COLORS", payload);
}
export async function loadShoppingCart({ commit }) {
  const response = await Axios.get("/api/purchases/cart");
  commit("SET_SHOPPING_CART", response.data);
}

export async function loadGuestShoppingCart({ commit }) {
  const response = await Axios.get("/api/purchases/cart");
  commit("SET_GUEST_SHOPPING_CART", response.data);
}

export async function loadVouchers({ commit }) {
  const response = await Axios.get("/api/discount/vouchers");
  commit("SET_VOUCHERS", response.data);
}

export async function loadCurrentUserProducts({ commit }) {
  const response = await Axios.get("/api/purchases");
  commit("SET_CURRENT_USER_PRODUCTS", response.data);
}
export async function loadAllUsersProducts({ commit }) {
  const response = await Axios.get("/api/purchases/all");
  commit("SET_ALL_USERS_PRODUCTS", response.data);
}

export async function loadAdminPreferences({ commit }) {
  const response = await Axios.get("/api/preferences");
  commit("SET_ADMIN_PREFERENCES", response.data);
}
