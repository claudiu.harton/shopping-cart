export function SET_BRANDNAME(state, payload) {
  state.brandName = payload;
}

export function SET_NOITEMS(state, payload) {
  state.brandName = payload;
}

export function SET_DETAILS(state, payload) {
  state.faq = payload.faq;
  state.aboutUs = payload.aboutUs;
  state.termsConditions = payload.termsConditions;
}

export function SET_CATEGORIES(state, payload) {
  state.categories = payload;
}
export function SET_PRODUCTS(state, payload) {
  state.products = payload;
}

export function SET_PROMOTIONS(state, payload) {
  state.promotions = payload;
}
export function SET_VOUCHER(state, payload) {
  state.voucher = payload;
}

export function SET_ONLINE_PAYMENT(state, payload) {
  state.onlinePayment = payload;
}

export function SET_SIZES(state, payload) {
  state.sizes = payload;
}

export function SET_COLORS(state, payload) {
  state.colors = payload;
}

export function SET_SHOPPING_CART(state, payload) {
  state.shoppingCart = payload;
}
export function SET_GUEST_SHOPPING_CART(state, payload) {
  state.guestShoppingCart = payload;
}
export function SET_VOUCHERS(state, payload) {
  state.vouchers = payload;
}

export function SET_CURRENT_USER_PRODUCTS(state, payload) {
  state.currentUserProducts = payload;
}

export function SET_ALL_USERS_PRODUCTS(state, payload) {
  state.allUsersProducts = payload;
}

export function SET_ADMIN_PREFERENCES(state, payload) {
  state.adminPreferences = payload;
}
