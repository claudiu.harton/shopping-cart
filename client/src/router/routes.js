const routes = [
  {
    path: "/admin",
    component: () => import("layouts/AdminLayout.vue"),
    children: [
      {
        path: "/",
        name: "dashboard",
        component: () => import("pages/admin/Dashboard.vue")
      },
      {
        path: "products",
        name: "products",
        component: () => import("pages/admin/Products.vue")
      },
      {
        path: "categories",
        name: "categories",
        component: () => import("pages/admin/Categories.vue")
      },

      {
        path: "business",
        name: "business",
        component: () => import("pages/admin/Business.vue")
      },
      {
        path: "customers",
        name: "customers",
        component: () => import("pages/admin/Customers.vue")
      },
      {
        path: "orders",
        name: "orders",
        component: () => import("pages/admin/Orders.vue")
      },
      {
        path: "promotions",
        name: "promotions",
        component: () => import("pages/admin/Promotions.vue")
      }
    ]
  },
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/categories/:category",
        name: "categoryPage",
        component: () => import("pages/client/CategoryPage.vue")
      },
      {
        path: "/",
        name: "home",
        component: () => import("pages/client/Home.vue")
      },
      {
        path: "/cart",
        name: "cart",
        component: () => import("pages/client/Cart.vue")
      },
      {
        path: "/billing",
        name: "billing",
        component: () => import("pages/client/Billing.vue")
      },
      {
        path: "/review",
        name: "review",
        component: () => import("pages/client/Review.vue")
      },
      {
        path: "/success",
        name: "success",
        component: () => import("pages/client/Success.vue")
      },
      {
        path: "/products/:product",
        name: "product",
        component: () => import("pages/client/Product.vue")
      },
      {
        path: "/profile",
        name: "profile",
        component: () => import("pages/client/Profile.vue")
      },
      {
        path: "/terms",
        name: "Terms & Conditions",
        component: () => import("pages/client/TermsConditions.vue")
      },
      {
        path: "/faq",
        name: "FAQ",
        component: () => import("pages/client/TermsConditions.vue")
      },
      {
        path: "/about",
        name: "About Us",
        component: () => import("pages/client/TermsConditions.vue")
      },
      {
        path: "/lost",
        name: "notFound",
        component: () => import("pages/Error404.vue")
      }
    ]
  },
  {
    path: "/auth",
    component: () => import("layouts/AuthLayout.vue"),
    children: [
      {
        path: "/",
        name: "auth",
        component: () => import("pages/Auth.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
